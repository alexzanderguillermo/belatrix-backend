package com.belatrix.backend.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.belatrix.backend.util.LogManager;

@RestController
@RequestMapping("/api")
public class BackendController {

	private LogManager logManager = LogManager.getLogger();

	@GetMapping("/log")
	public ResponseEntity<String> tryOne() {

		try {
			logManager.message("A");
			logManager.warn("B");
			logManager.error("C");
		}
		catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}

}
