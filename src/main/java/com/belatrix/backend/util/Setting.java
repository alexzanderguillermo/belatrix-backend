package com.belatrix.backend.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Setting {

	private final static Properties	config	= new Properties();
	private static String			path;
	private static Setting			instance;

	private Setting() {
		path = "src/main/resources/logger.properties";

		FileInputStream configFileStream;

		try {
			configFileStream = new FileInputStream(path);
			try {
				config.load(configFileStream);
				configFileStream.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}

	public static Setting getInstance() {
		if (instance == null) {
			instance = new Setting();
		}
		return instance;
	}
	
	public Properties getProperties() {
		return config;
	}

}
