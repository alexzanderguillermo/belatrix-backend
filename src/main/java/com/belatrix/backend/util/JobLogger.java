package com.belatrix.backend.util;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public abstract class JobLogger {

	protected static boolean		logToFile;
	protected static boolean		logToConsole;
	protected static boolean		logMessage;
	protected static boolean		logWarning;
	protected static boolean		logError;
	protected static boolean		logToDatabase;

	protected Map<String, String>	dbParams;
	protected Logger				logger;

	protected FileHandler			fileHandler;
	protected JobDatabase			jobDatabase;
	protected ConsoleHandler		consoleHandler;

	protected void setupConfig() throws SQLException, IOException {
		Properties properties = Setting.getInstance().getProperties();
		
		logger = Logger.getLogger("MyLog");

		dbParams = new HashMap<>();

		if (properties.containsKey("userName")) {
			dbParams.put("userName", properties.getProperty("userName"));
		}

		if (properties.containsKey("password")) {
			dbParams.put("password", properties.getProperty("password"));
		}

		if (properties.containsKey("dbms")) {
			dbParams.put("dbms", properties.getProperty("dbms"));
		}

		if (properties.containsKey("serverName")) {
			dbParams.put("serverName", properties.getProperty("serverName"));
		}

		if (properties.containsKey("portNumber")) {
			dbParams.put("portNumber", properties.getProperty("portNumber"));
		}

		if (properties.containsKey("logFileFolder")) {
			dbParams.put("logFileFolder", properties.getProperty("logFileFolder"));
		}

		if (properties.containsKey("logMessage")) {
			logMessage = Boolean.parseBoolean(properties.getProperty("logMessage"));
		}

		if (properties.containsKey("logWarning")) {
			logWarning = Boolean.parseBoolean(properties.getProperty("logWarning"));
		}

		if (properties.containsKey("logError")) {
			logError = Boolean.parseBoolean(properties.getProperty("logError"));
		}

		if (properties.containsKey("logToFile")) {
			logToFile = Boolean.parseBoolean(properties.getProperty("logToFile"));
		}

		if (properties.containsKey("logToConsole")) {
			logToConsole = Boolean.parseBoolean(properties.getProperty("logToConsole"));
		}

		if (properties.containsKey("logToDatabase")) {
			logToDatabase = Boolean.parseBoolean(properties.getProperty("logToDatabase"));
		}
		
		if (!logToConsole && !logToFile && !logToDatabase) {
			throw new IllegalArgumentException("Invalid configuration");
		}

		if (!logError && !logMessage && !logWarning) {
			throw new IllegalArgumentException("Error or Warning or Message must be specified");
		}
	}

	protected void setupHandlers() throws SQLException, IOException {
		logger.setUseParentHandlers(false);

		if (logToDatabase) {
			jobDatabase = new JobDatabase(dbParams);
			jobDatabase.createTable();
		}

		if (logToFile) {
			fileHandler = connectFileHandler();
		}

		if (logToConsole) {
			consoleHandler = new ConsoleHandler();
		}
	}
	
	protected boolean isMessageValid(String messageText) {
		return messageText != null && !messageText.trim().isEmpty();
	}

	protected boolean isValidateLogType(LogType logType) {
		boolean isValid = false;

		if (LogType.MESSAGE.equals(logType) && logMessage) {
			isValid = true;
		}

		if (LogType.WARN.equals(logType) && logWarning) {
			isValid = true;
		}

		if (LogType.ERROR.equals(logType) && logError) {
			isValid = true;
		}

		return isValid;
	}

	protected FileHandler connectFileHandler() throws IOException {
		FileHandler fileHandler = null;

		File logFile = new File(dbParams.get("logFileFolder") + "/logFile.txt");
		if (!logFile.exists()) {
			logFile.createNewFile();
		}

		if (fileHandler == null) {
			fileHandler = new FileHandler(dbParams.get("logFileFolder") + "/logFile.txt", true);
		}

		return fileHandler;
	}

}
