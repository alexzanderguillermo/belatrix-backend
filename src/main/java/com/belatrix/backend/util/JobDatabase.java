package com.belatrix.backend.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Properties;

public class JobDatabase {

	private Map<String, String> dbParams;

	public JobDatabase(Map<String, String> dbParams) throws SQLException {
		this.dbParams = dbParams;
	}

	private Connection createConnection() throws SQLException {
		Properties connectionProps = new Properties();
		connectionProps.put("user", dbParams.get("userName"));
		connectionProps.put("password", dbParams.get("password"));

		return DriverManager.getConnection("jdbc:" + dbParams.get("dbms") + ":" + dbParams.get("serverName"), connectionProps);
	}

	public void createTable() throws SQLException {
		String sql = "CREATE TABLE IF NOT EXISTS log_values (message VARCHAR(255), type VARCHAR(255), date VARCHAR(255))";

		try (Connection connection = createConnection(); Statement statement = connection.createStatement();) {
			statement.executeUpdate(sql);
		}
		catch (Exception e) {
			throw e;
		}
	}

	public void dropTable() throws SQLException {
		String sql = "DROP TABLE IF EXISTS log_values";

		try (Connection connection = createConnection(); Statement statement = connection.createStatement();) {
			statement.executeUpdate(sql);
		}
		catch (Exception e) {
			throw e;
		}
	}

	public void insertTable(String messageText, LogType logType, String currentDate) throws SQLException {
		String sql = "INSERT INTO log_values(message, type, date) VALUES (?,?,?)";

		try (Connection conn = createConnection(); PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
			preparedStatement.setString(1, messageText);
			preparedStatement.setString(2, logType.val().toString());
			preparedStatement.setString(3, currentDate);
			preparedStatement.executeUpdate();
		}
		catch (SQLException e) {
			throw e;
		}
	}

}
