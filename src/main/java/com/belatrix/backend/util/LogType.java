package com.belatrix.backend.util;

import java.util.logging.Level;

public enum LogType {

	MESSAGE(Level.INFO), WARN(Level.WARNING), ERROR(Level.SEVERE);

	private Level level;

	private LogType(Level level) {
		this.level = level;
	}

	public Level val() {
		return level;
	}

}
