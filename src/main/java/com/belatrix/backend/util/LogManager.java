package com.belatrix.backend.util;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;

public class LogManager extends JobLogger {

	private static LogManager instance;

	public static LogManager getLogger() {
		if (instance == null) {
			try {
				instance = new LogManager();
			}
			catch (SQLException | IOException e) {
				e.printStackTrace();
			}
		}

		return instance;
	}

	private LogManager() throws SQLException, IOException {
		setupConfig();
		setupHandlers();
	}

	public void message(String msg) throws SQLException {
		log(msg, LogType.MESSAGE);
	}

	public void warn(String msg) throws SQLException {
		log(msg, LogType.WARN);
	}

	public void error(String msg) throws SQLException {
		log(msg, LogType.ERROR);
	}

	private void log(String messageText, LogType logType) throws SQLException {
		if (!isMessageValid(messageText)) {
			throw new IllegalArgumentException("Error with message");
		}

		if (!isValidateLogType(logType)) {
			return;
		}

		messageText = messageText.trim();
		String currentDate = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(new Date());

		if (logToFile || logToConsole) {
			String finalMsg = logType.val() + " " + currentDate + " " + messageText;

			if (logToFile) {
				logger.addHandler(fileHandler);
				logger.log(logType.val(), finalMsg);
				logger.removeHandler(fileHandler);
			}

			if (logToConsole) {
				logger.addHandler(consoleHandler);
				logger.log(logType.val(), finalMsg);
				logger.removeHandler(consoleHandler);
			}
		}

		if (logToDatabase) {
			jobDatabase.insertTable(messageText, logType, currentDate);
		}
	}

}
