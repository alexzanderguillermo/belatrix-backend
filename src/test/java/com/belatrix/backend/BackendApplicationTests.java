package com.belatrix.backend;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.SQLException;
import java.util.Properties;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.belatrix.backend.util.LogManager;
import com.belatrix.backend.util.Setting;

@RunWith(SpringRunner.class)
class BackendApplicationTests {

	@Test
	void testPropertiesHandlersAreFalse() {
		Properties prop = Setting.getInstance().getProperties();
		prop.setProperty("logToConsole", "false");
		prop.setProperty("logToDatabase", "false");
		prop.setProperty("logToFile", "false");
		prop.setProperty("logWarning", "true");
		prop.setProperty("logMessage", "true");
		prop.setProperty("logError", "true");
		
		assertThrows(IllegalArgumentException.class, () -> {
			LogManager logManager = LogManager.getLogger();
	    });
	}
	
	@Test
	void testPropertiesLogTypesAreFalse() {
		Properties prop = Setting.getInstance().getProperties();
		prop.setProperty("logToConsole", "true");
		prop.setProperty("logToDatabase", "true");
		prop.setProperty("logToFile", "true");
		prop.setProperty("logWarning", "false");
		prop.setProperty("logMessage", "false");
		prop.setProperty("logError", "false");
		
		assertThrows(IllegalArgumentException.class, () -> {
			LogManager logManager = LogManager.getLogger();
	    });
	}

	@Test
	void testMessageTextIsNull() throws SQLException {
		assertThrows(IllegalArgumentException.class, () -> {
			LogManager logManager = LogManager.getLogger();
			logManager.message(null);
	    });
	}
	
}
